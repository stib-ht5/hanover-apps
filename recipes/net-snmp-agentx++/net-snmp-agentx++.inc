DESCRIPTION = "7833 firmware loader"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "GPLv2"

DEPENDS += "boost net-snmp"

SRCREV = "${PV}"
BRANCH = "master"

INC_PR = "r2"
PR = "${INC_PR}"

S = "${WORKDIR}/git"

inherit autotools 

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/embedded-apps/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.0.8.md5sum] = "31e24dbdc94864b99fddbe3318c21e30"
SRC_URI[v1.0.8.sha256sum] = "608dbbe6ecb773cdde486757454458a8122198193b4e230625c3bc49a869f551"
