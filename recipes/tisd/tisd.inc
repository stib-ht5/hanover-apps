DESCRIPTION = "HTC Hardware Testing Suite"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "Proprietary"

DEPENDS += "libhtc-core"

inherit autotools pkgconfig

INC_PR = "r1"
PR = "${INC_PR}"

FILES_${PN}-dbg += "${libexecdir}/*/.debug"

# runit service script
do_install_append() {
	local svdir="${D}${sysconfdir}/sv/${PN}"
	mkdir -p "$svdir/log"
	cat <<EOT > "$svdir/run"
#!/bin/sh
exec 2>&1
exec ${sbindir}/${PN} -p23
EOT
	chmod +x "$svdir/run"
	ln -snf "../../logrotate/log/run" "$svdir/log/run"
}
