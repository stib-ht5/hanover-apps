DESCRIPTION = "On-board computer sofware package"
HOMEPAGE = "http://amuxi/wiki/index.php/HTC_HASL_Message_Directory"
SECTION = "console/misc"
LICENSE = "GPLv2"

inherit autotools 

SRC_URI = "git://${HANOVER_GIT_LEGACY}/embedded-apps/${PN}.git;protocol=git;branch=${BRANCH}"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"
