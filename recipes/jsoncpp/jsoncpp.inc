DESCRIPTION = "HTC, IP LED render"
LICENSE = "GPLv2"
PR="r1"

inherit cmake

EXTRA_OECMAKE = "-DBUILD_SHARED_LIBS=ON \
                 -DBUILD_STATIC_LIBS=OFF \
                 -DJSONCPP_WITH_TESTS=OFF \
                "

SRC_URI = "https://github.com/open-source-parsers/${PN}/archive/${PV}.tar.gz"
