DESCRIPTION = "HTC Classic"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "misc"
LICENSE = "Proprietary"

inherit autotools

INC_PR = "r3"
PR ?= "${INC_PR}"

SRC_URI = "git://${HANOVER_GIT}/htc/hanover-utils.git;protocol=http;branch=${BRANCH}"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

DEPENDS += "libhtc-core"
EXTRA_OECONF += "--program-transform-name s/^bin_//"

FILES_${PN}-dbg += "${libexecdir}/${PN}/.debug"
