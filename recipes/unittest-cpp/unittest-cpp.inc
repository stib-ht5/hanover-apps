DESCRIPTION = "UnitTest++ is a unit testing framework for C++"
HOMEPAGE = "http://sourceforge.net/p/unittest-cpp/wiki/Home/"
SECTION = "console/misc"
LICENSE = "GPLv2"

DEPENDS = ""

SRCREV = "${PV}"
BRANCH = "master"

PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/oss/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.4.md5sum] = "e855af64ca127efc7f56e4591dd4674d"
SRC_URI[v1.4.sha256sum] = "87f93f195ba422c2d7d01dd075a0f0e585cc0ed3fb55872b05606e5b280011b0"

S = "${WORKDIR}/git"

do_configure() {
  :
}

do_compile () {
 oe_runmake libUnitTest++.a
 }
 
do_install() {
 install -d ${D}${libdir}
 install -d ${D}${includedir}/unittest++
 install -d ${D}${includedir}/unittest++/Posix
 
 install ${S}/libUnitTest++.a  ${D}${libdir}/
 install ${S}/src/UnitTest++.h ${D}${includedir}/unittest++/
 install ${S}/src/*.h          ${D}${includedir}/unittest++/
 install ${S}/src/Posix/*.h    ${D}${includedir}/unittest++/Posix/
}

FILES_${PN} = "${libdir}"
FILES_${PN}-dev = "${libdir} ${includedir}"
 
