DESCRIPTION = "Hanover system python scripts"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "GPLv2"

DEPENDS = "python"

SRCREV = "${PV}"
BRANCH = "master"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/embedded-apps/${PN}.git;protocol=git;branch=${BRANCH}"

S = "${WORKDIR}/git"

do_configure() {
	:
}

do_compile() {
	:
}

do_install () {
	install -d ${D}${bindir} 
    
	install -m 0755 ${S}/* ${D}${bindir}
}

FILES_${PN} = "${bindir}"
