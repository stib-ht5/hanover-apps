DESCRIPTION = "7833 firmware loader"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "GPLv2"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

INC_PR = "r1"
PR = "${INC_PR}"

inherit autotools 

SRC_URI = "git://${HANOVER_GIT_LEGACY}/embedded-apps/${PN}.git;protocol=git;branch=${BRANCH}"

PACKAGES =+ "${PN}-7833"
PACKAGES =+ "${PN}-signs"

FILES_${PN}-7833 = "${bindir}/${PN}-7833"
FILES_${PN}-signs = "${bindir}/${PN}-signs"
