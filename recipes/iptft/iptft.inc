# vim: set ft=sh ts=4 sw=4 et:
DESCRIPTION = "HTC, IP LED render"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "Proprietary"

INC_PR = "r0"
PR ?= "${INC_PR}"

DEPENDS = "libsdl-gfx libsdl-image libsdl-mixer libsdl-ttf libsdl-net tiff jsoncpp"
DEPENDS += "gstreamer curl unittest-cpp"

DEPENDS_append_dm814x-z3 = " z3-camclient"
EXTRA_OECONF_append_dm814x-z3 = " --with-videostreaming=z3"

inherit autotools

EXTRA_OECONF += ${@base_contains('DISTRO_FEATURES', 'runit', ' --enable-runit', '', d)}

imagesdir=/usr/share/images

FILES_${PN}-dbg += "${libexecdir}/${PN}/.debug"

do_install_append () {
    install -d ${D}${sysconfdir}/init.d
    install -d ${D}${sysconfdir}/rc5.d
    install -d ${D}${bindir}
    install -d ${D}${imagesdir}

    rsync -av ${S}/images/*                       ${D}${imagesdir}/

    install  ${S}/scripts/ignition_off.sh         ${D}${bindir}/
	install  ${S}/scripts/ignition_on.sh          ${D}${bindir}/
	install  ${S}/scripts/ignition_set_state.sh   ${D}${bindir}/
	install  ${S}/scripts/read_manuf_info.sh      ${D}${bindir}/
	install  ${S}/scripts/set_manuf_info.sh       ${D}${bindir}/
	install  ${S}/scripts/bootmonitor.sigusr2.execute.sh  ${D}${bindir}/
    install  ${S}/scripts/bootmonitor.sigusr2.prepare.sh  ${D}${bindir}/

    echo "0" > ${D}${sysconfdir}/tftmode.conf

    rm -f ${D}${sysconfdir}/rc5.d/S80tftmode
}

FILES_${PN}_append = " ${imagesdir} ${sysconfdir} ${bindir}"
FILES_${PN}-dev_append = " ${libdir}/tftrender/*.a"
CONFFILES_${PN} = "${sysconfdir}/tftmode.conf"

PROVIDES += "tftrender"
PACKAGES  = "tftrender-dev tftrender-static tftrender"
PACKAGES += "${PN}-dbg ${PN}"

FILES_tftrender-dev     = "${libdir}/pkgconfig/tftrender.pc"
FILES_tftrender-dev    += "${includedir}/tftrender"
FILES_tftrender-static  = "${libdir}/tftrender/*.a"
