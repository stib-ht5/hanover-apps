require iptft.inc

EXTRA_OECONF += ${@base_contains('DISTRO_FEATURES', 'runit', ' --enable-runit', '', d)}

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/embedded-apps/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.3.1.md5sum] = "79438591dba321823c5c86f4861027bd"
SRC_URI[v1.3.1.sha256sum] = "4290dbf6bd9bf7e682a56b163bdd9a9fe8327770b456b20ff61d5c1618f6308c"

