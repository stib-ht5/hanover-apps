DESCRIPTION = "C++ wrapper for the Sqlite database C API"
HOMEPAGE = "http://www.alhem.net/project/sqlite"
SECTION = "libs"
LICENSE = "GPL"

DEPENDS = "sqlite3"
RDEPENDS = "sqlite3"


#EXTRA_OEMAKE = "CROSSCOMPILE=${TARGET_PREFIX}"
CFLAGS += " -fPIC"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/oss/${PN}.git;protocol=git;branch=${BRANCH}"

SRCREV = "${PV}"
BRANCH = "master"
S = "${WORKDIR}/git"

PR = "r1"

#No configuration is required
do_configure() {
 :
 }
  
do_compile() {
  oe_runmake 
 }

do_install() {
 install -d ${D}${libdir}
 install -d ${D}${includedir}
 
 install ${S}/libsqlitewrapped.a ${D}${libdir}/
 install ${S}/libsqlitewrapped.h ${D}${includedir}/
}

FILES_${PN} = "${libdir}"
FILES_${PN}-dev = "${libdir} ${includedir}"
