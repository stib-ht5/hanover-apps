#!/bin/sh

# file timesync.sh
# STIB HASL time sync
# Author  S.Elliott
# Date    22 May 2014
# See SR1192 (STIB) Software Requirement Specifications                                              
# section 6.2: Time synchronisation

# timeserver IP address
TIMESERVER="10.0.0.1"

# A single time sync and hardware clock set
set_time()
{
	dt=$(date)
	echo "timesync.sh: synchronise time to $TIMESERVER at $dt"
	if (ntpdate -s $TIMESERVER); then
		echo "timesync.sh: Successfully synchronised time. Setting hardware clock"
		hwclock --systohc --utc 
		echo "timesync.sh: hardware clock set"
        echo 1 > /tmp/timeok
	else
		echo "timesync.sh: Could not synchronise time. Check syslog"
        echo 0 > /tmp/timeok
	fi	
}

# main function
time_sync_main()
{
	# time sync once per minute for first 15 minutes
	for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
	do
		set_time
		sleep 60
	done
	
	# time sync once per hour thereafter
	while true
	do
		set_time
		sleep 3600
	done
}

# run main function in background!
time_sync_main &
