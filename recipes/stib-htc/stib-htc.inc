DESCRIPTION = "STIB HTC5 embedded software"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "GPLv2"

DEPENDS = "jsoncpp sqlite3 hasl net-snmp-agentx++"

INC_PR = "r3"
PR = "${INC_PR}"

inherit autotools

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/embedded-apps/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.1.4.md5sum] = "ab9e1b8bce84b0b92a8fca9afe485542"
SRC_URI[v1.1.4.sha256sum] = "e3e086626fce5a2ce4c8650f1c19b5b6ee914ebcdbec2caa47b2f5f6cd30b27d"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"
EXTRA_OECONF += " --enable-silent-rules"
EXTRA_OECONF += ${@base_contains('DISTRO_FEATURES', 'runit', ' --enable-runit', '', d)}

STIB_FILES="${OEBASE}/hanover-apps/recipes/stib-htc/files"

do_install_append() {
   install -d ${D}${sysconfdir}/init.d
   install -d ${D}${sysconfdir}/rc5.d

   install -m 0755 ${STIB_FILES}/timesync.sh ${D}${sysconfdir}/init.d

   ln -sf ../init.d/timesync.sh ${D}${sysconfdir}/rc5.d/S99timesync.sh
}

FILES_${PN} = "${sysconfdir}/init.d ${sysconfdir}/rc5.d ${sysconfdir}/sv ${bindir}"
