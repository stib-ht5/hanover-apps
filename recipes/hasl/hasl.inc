DESCRIPTION = "HASL - Hanover Application and Services Library"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "GPLv2"

DEPENDS = "jsoncpp sqlite3 net-snmp-agentx++ unittest-cpp"

inherit autotools 

INC_PR = "r3"
PR ?= "${INC_PR}"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/embedded-apps/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.1.6.md5sum] = "8e32a1d7bbe3a3c1c710720b63cd3c45"
SRC_URI[v1.1.6.sha256sum] = "d6031ac90e6b4aaddcde9e16c0601fb138ac1379bf79e9f8742b91f3fceb2da8"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"
EXTRA_OECONF = "--enable-silent-rules"
