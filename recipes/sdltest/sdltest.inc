DESCRIPTION = "Graphic SDL sample application"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "GPLv2"

DEPENDS = "libsdl-gfx libsdl-image libsdl-mixer libsdl-ttf libsdl-net"

inherit autotools 

SRC_URI = "git://${HANOVER_GIT_LEGACY}/embedded-apps/${PN}.git;protocol=git;branch=${BRANCH}"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"
