DESCRIPTION = "HTC Classic"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "misc"
LICENSE = "Proprietary"

inherit autotools

INC_PR = "r2"
PR ?= "${INC_PR}"

SRC_URI = "git://${HANOVER_GIT}/htc/${PN}.git;protocol=http;branch=${BRANCH}"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

DEPENDS += "libsdl-gfx libsdl-image libsdl-mixer libsdl-ttf libsdl-net tiff"
DEPENDS += "libhtc-core"

EXTRA_OECONF += "--program-transform-name s/^bin_//"

FILES_${PN}-dbg += "${libexecdir}/${PN}/.debug"
