DESCRIPTION = "HSLIB"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "library/misc"
LICENSE = "Proprietary"

inherit autotools

INC_PR = "r0"
PR = "${INC_PR}"

SRC_URI = "git://${HANOVER_GIT}/htc/${PN}.git;protocol=http;branch=${BRANCH}"

# needed for m4 macros
DEPENDS += "libhtc-core"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

FILES_${PN}-dbg += " ${libexecdir}/htc/.debug"

