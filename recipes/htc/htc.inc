DESCRIPTION = "HTC Classic"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "misc"
LICENSE = "Proprietary"

inherit autotools

INC_PR = "r1"
PR = "${INC_PR}"

S = "${WORKDIR}/git"

#libraries
DEPENDS += "jsoncpp unittest-cpp lua5.1"
DEPENDS += "hslib tftrender sqlitewrapped libhtc-core libhtc-audio libhtc-hcp"
DEPENDS += "gstreamer sqlite3"
DEPENDS += "curl"

DEPENDS_append_dm365-htc = " htc-camserv gstreamer-ti"
DEPENDS_append_dm365-htc += " acapela-babile"

# command line helpers
RDEPENDS += "mpg123 sqlite3"

EXTRA_OECONF += "--program-transform-name s/^bin_//"

# iconv
#
# cp1250..cp1258
RDEPENDS += "${@' '.join([ 'eglibc-gconv-cp%u' % cp for cp in range(1250, 1259)])}"

FILES_${PN}-dbg += "${libexecdir}/${PN}/.debug"

do_deploy[dirs] = "${B}"
addtask deploy before do_package_stage after do_install

do_deploy(){
    set -x
    install -D -m 0644 src/mftupdate.bin "${DEPLOY_DIR_IMAGE}/pendrive/mftupdate.bin"
    install -D -m 0755 ${S}/src/hcsync/apps/mftupdate.sh "${DEPLOY_DIR_IMAGE}/pendrive/mftupdate.sh"
    install -D -m 0755 ${S}/src/hcsync/apps/update.sh "${DEPLOY_DIR_IMAGE}/pendrive/update.sh"
}

