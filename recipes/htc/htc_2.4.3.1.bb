require ${PN}.inc

SRC_URI = "\
        git://${HANOVER_GIT}/htc/${PN}.git;protocol=http;branch=${BRANCH} \
"

SRCREV = "${PV}"
BRANCH = "master"
