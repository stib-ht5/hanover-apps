DESCRIPTION = "Z3 Camera Client library, libz3-camclient"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "GPLv2"

COMPATIBLE_MACHINE = "(dm814x-z3)"
PACKAGE_ARCH = "${MACHINE_ARCH}"
DEPENDS = "gstreamer"

PR = "r1"

inherit autotools 

export CFLAGS = "-I${STAGING_DIR_HOST}/usr/include/gstreamer-0.10 \
                 -I${STAGING_DIR_HOST}/usr/include/glib-2.0 \
                 -I${STAGING_DIR_HOST}/usr/lib/glib-2.0/include \
                 -I${STAGING_DIR_HOST}/usr/include/libxml2 \ 
                 "

SRC_URI = "git://${HANOVER_GIT_LEGACY}/embedded-apps/${PN}.git;protocol=git;branch=${BRANCH}"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"
