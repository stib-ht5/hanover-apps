DESCRIPTION = "HTC Camera Server library, libhtc-camserv"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "console/misc"
LICENSE = "Propriertary"

inherit autotools

INC_PR = "r1"
PR ?= "${INC_PR}"

COMPATIBLE_MACHINE = "dm365-htc"
PACKAGE_ARCH = "${MACHINE_ARCH}"

DEPENDS += "ti-dmai"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/embedded-apps/${PN}.git;protocol=git;branch=${BRANCH}"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

EXTRA_OECONF_append += "DVSDK_DIR=${STAGING_DIR}/${MULTIMACH_TARGET_SYS}/usr/share/ti"
EXTRA_OECONF_append += "--enable-test"
EXTRA_OECONF_append += "--enable-example"

PACKAGES  = "${PN}-tests ${PN}-tests-dbg"
PACKAGES += "${PN}-dev ${PN} ${PN}-dbg"

FILES_${PN}-tests += "${bindir}/*"
FILES_${PN}-tests-dbg += "${bindir}/.debug"
